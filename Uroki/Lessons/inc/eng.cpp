//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//Инкримент.While.
//V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

#include<iostream>
using namespace std;

int main() {
	char eng = 'a';
	while (eng <= 'z') {
		++eng;
		cout << eng  << " ";
	}
	cout << " " << endl;
	
	return 0;
}
//Output
/*

*/
//=-===-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//end file
//-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=//
