//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//Булевый тип данных.
//V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

#include<iostream>
using namespace std;

int main() {
	cout << boolalpha;
	bool a = false;
	bool b = true;
	cout << a << "\t" <<  b <<  "\n";
	int aa = 5, bb = 12;
	bool result = ( aa == bb ) ? true : false;
	cout << "аа = бб?" << result << endl;
	result = (aa < bb ) ? true : false;
	cout << "аа < бб?" << result << endl;
	result = (aa > bb ) ? true : false;
	cout << "аа > бб?" << result << endl;
	result = (aa <= bb ) ? true : false;
	cout << "аа <= бб?" << result << endl;
	result = (aa >=  bb ) ? true : false;
	cout << "аа >=  бб?" << result << endl;
	result = (aa != bb) ? true : false;
	cout << "aa не равно вв " << result << endl;
	result = (aa < bb ) || (aa > bb ) ;
	cout << "ili " << result << "\n";
	result = (aa < bb) && (aa > bb) ;
	cout << " Сравнение ИИ " << result << endl;
	result = (aa != bb ) xor (aa < bb);
	cout << " Сравнение XOR " << result << endl;
	return 0;
} 
//Output
/*
false	true
аа = бб?false
аа < бб?true
аа > бб?false
аа <= бб?true
аа >=  бб?false
aa не равно вв true
ili true
 Сравнение ИИ false
 Сравнение XOR false
 
*/
//=-===-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//end file
//-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=//

