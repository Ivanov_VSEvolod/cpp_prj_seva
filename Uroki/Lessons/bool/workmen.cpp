//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//Булевый тип данных.
//V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

#include<iostream>
using namespace std;

int main() {
	cout <<  boolalpha;
	cout << "Попадет ли людь на работу, если: " << endl;
	bool working = true;
	cout << "Дождь ? ";
	bool rain ;
	cin >> rain ;
	cout << "Zont ? ";
	bool zont ;
	cin >> zont ;
	cout << "bus ? ";
	bool bus; 
	cin >> bus ;
	working = bus &&  (!rain ||  zont) ;
	cout << "rabota ? " << working << endl;
	
	
	return 0;
} 
//Output
/*
 Попадет ли людь на работу, если: 
 Дождь ? 1
 Zont ? 0
 bus ? 0
 rabota ? false
 
*/
//=-===-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//end file
//-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=//
