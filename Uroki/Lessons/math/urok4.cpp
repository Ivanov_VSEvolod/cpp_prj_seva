//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//Примеры строк и работа с ними, а также арифметика
///V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//
#include<iostream>
#include<unistd.h>
using namespace std;

int main() {
	cout << boolalpha;
	char strSeva [] ={" Seva "} ;
	char strProg [] ={" programmer "} ;
	cout << strSeva  << " \t " << strProg << endl;
	cout << (sizeof(strProg) / sizeof(strProg[0])) << "\n";
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= \n";
	string strJamesString = "James Bond";
	string strZeroString = " 007 ";
	cout << strJamesString + strZeroString << endl;
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= \n";
	int a = 5, b = 6;
	cout << a + b << endl;
	int result = a +( b - 3 );
	cout << " a + (b - 3)  = " << result << endl;
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= \n";
	int aa = 5, bb = 6;
	int res = aa * bb;
	cout << " aa * bb = " << res << endl;
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= \n";
	int aaa = 12, bbb = 2;
	int devision = aaa / bbb;
	cout << "aaa : bbb = " << devision << endl;
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= \n";
	int val = 6 + 5 - (1 * (5 / 2));
	cout << val << endl;
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= \n";
	int a2 = 5, b2 = 6;
	cout << (a2 += b2) << "\n";
	cout << (a2 -= b2) << "\n";
	cout << (a2 *= b2) << "\n";
	cout << (a2 /= b2) << "\n";
	cout << "Целое число? " << ((20 % 2 ) ? false : true ) << endl;
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= \n";
	
	sleep (2);
    cout << " Сигнал с задержкой" << "\7" << endl;
	return 0;
}
//Output
/*
 Seva  	  programmer 
13
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
James Bond 007 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
11
 a + (b - 3)  = 8
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
 aa * bb = 30
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
aaa : bbb = 6
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
9
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
11
5
30
5
Целое число? true
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
 Сигнал с задержкой

*/
//=-===-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//end file
//-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

