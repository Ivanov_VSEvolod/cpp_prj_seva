//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//Массивы.
//V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

#include<iostream>
using namespace std;

int main() {
	srand(static_cast<unsigned int>(time(0)));
	int arr[5] = {};
	int secretNumber;
	for (int i = 0; i <= 4; ++i) {
		secretNumber = (rand() % 15) + 0;
		arr[i] = secretNumber;
	}
	cout << "\nСодержимое массива are: ";
	for (int s = 0; s <= 4; ++s) {
		cout << arr[s] << " ";
	}
	cout << "\n";
	return 0;
}
//Output
/*

*/
//=-===-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//end file
//-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=//
