//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//Двухмерный массив.
//V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

#include<iostream>
using namespace std;

int main() {
	const int  ARR_COL_SIZE = 3, ARR_ROW_SIZE = 3;
	int multiArr[ARR_ROW_SIZE][ARR_COL_SIZE] = {{12, 30, 45}, {78, 96, 101}};
	cout << "Доступ к многомерным массивам Обычным спосоьом: " ;
	cout << multiArr[0][0] << ", ";//1р 1к
	cout << multiArr[0][1] << ", ";//1р 2к
	cout << multiArr[0][2] << ", ";//1р 3к
	cout << multiArr[1][0] << ", ";//2р 1к
	cout << multiArr[1][1] << ", ";//2р 2к
	cout << multiArr[1][2] << ", ";//2р 3к
	cout << endl;
	cout << "Доступ к многомерным массивам Обычным спосоьом: ";
	for (int row = 0; row < 2; ++row){
		for (int col = 0; col < 3; ++col){
			cout << multiArr[row][col] << " ";
		}	
		cout << "\t";
	}
	cout << "\nПолучение x/y координат\n";
	cout << "X: ";
	for (int x = 0; x < 3; ++x) {
		cout << multiArr[0][x] << " ";
	}
	cout << "\n Y: ";
	for (int y = 0; y < 3; ++y) {
		cout << multiArr[1][y] << " ";
	}
	cout << endl;
	return 0;
}
//Output
/*

*/
//=-===-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//end file
//-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=//
	
