//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//String
//V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

#include<iostream>
#include<string>
using namespace std;

int main() {
	char mystring[] = "String";
	cout << mystring << " содержит: " << sizeof(mystring) << endl;
	cout << "ASCII код каждого элемента строки: ";
	for (unsigned index = 0; index < (sizeof(mystring)); ++index) {
		cout << static_cast<int>(mystring[index]) << " ";
	}
	cout << endl;
	cout << "Изменить элемент строки\n"; 
	mystring[1] = 'P';
	cout << mystring << endl;
	cout << "Строки с помощью класса string\n";
	string str = "Ivan";
	string more = str;
	cout << more << endl;
	cout << "Сложение строк\n";
	string firstName, lastName;
	cout << "Введите ваше имя: ";
	cin >> firstName;
	cout << "Введите ваше фамилию: ";
	cin >> lastName;
	string con = firstName + lastName;
	cout << con << endl;
	cout << "Сравним слова\n";
	cout << "Введите 1-е слово: ";
	string firstWord;
	cin >> firstWord;
	cout << "Введите 2-е слово: ";
	string secondWord;
	cin >> secondWord;
	if (firstWord != secondWord) {
		cout << "Cлова разные\n";
	}
	else {
		cout << "Слова одинаковые\n";
	}
	string num1 = "35";
	string num2 = "20";
	cout << num1 << num2 << endl;
	cout << "Enter a World";
	string userWorld;
	getline(cin, userWorld);
	cout << "Length your World" << userWorld.length() << "characters\n";
	
	return 0;
}
//Output
/*

*/
//=-===-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//end file
//-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=//
