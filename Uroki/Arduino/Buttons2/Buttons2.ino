void setup() {
  Serial.begin(9600);
}

void loop() {
  bool touch1 = digitalRead(3);
  Serial.print("Touch 1: ");
  Serial.print(touch1);
  bool touch2 = digitalRead(4);
  Serial.print(" Touch 2: ");
  Serial.print(touch2);
  bool touch3 = digitalRead(5);
  Serial.print(" Touch 3: ");
  Serial.print(touch3);
  bool touch4 = digitalRead(6);
  Serial.print(" Touch 4: ");
  Serial.println(touch4);
  delay(40);
}
