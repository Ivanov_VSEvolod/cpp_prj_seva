const int buttonPin = 4; // Выставляем значения порта, подсоединённого с сигнал-портом кнопки

int count = 0; // Переменная, предназначенная для выбора режима работы


void setup() {

pinMode(LED_BUILTIN, OUTPUT); // Команда для адекватного реагирования светодиода

pinMode(buttonPin, INPUT); // Открываем порт для считывания

}

void loop() {

if(digitalRead(buttonPin)){ // При нажатии кнопки...

count = count + 1; // Изменяем режим кнопки

if(count > 2){ //В случае превышения значения count начинаем отсчет сначала

count = 0;

}

while(digitalRead(buttonPin)){ // Пустой цикл для ожидания, пока пользователь отпустит кнопку

}

}

if(count == 0) { // 3 режима по переключению кнопки:

digitalWrite(LED_BUILTIN, LOW); // 1: Выключенный светодиод


} else {

digitalWrite(LED_BUILTIN, HIGH); // 3: Мигающий

delay(100);

digitalWrite(LED_BUILTIN, LOW);

delay(100);

}

}
