const int PINS[] = {3, 5, 6, 7, 8, 9, 10, 11}; // 0...

void setup() {
  for (int a = 0; a <= 7; ++a) {
    pinMode(PINS[a], OUTPUT);
  }

}

void loop() {
  /*
    for (int i = 0; i <= 7; ++i) {
     digitalWrite(PINS[i], !digitalRead(PINS[i]));
     delay(100);
    }
  */
  int RANDOM = random(0, 8);
  digitalWrite(PINS[RANDOM], !digitalRead(PINS[RANDOM]));
  delay(100);
}
