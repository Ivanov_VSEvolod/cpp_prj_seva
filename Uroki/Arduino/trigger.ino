// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// 
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
void setup() {
  Serial.begin(9600);

}

void loop() {
  static bool currient = false; // Флаг текущего состояния 
  static bool previous = false; // Флаг предыдущего состояния
  bool buttonState = digitalRead(3); // Считываем состояние кнопки
  if (previous == true && buttonState == false) { // Условие
    currient = !currient; // Флаг делаем true
    Serial.println(currient);
  }
  previous = buttonState;
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
