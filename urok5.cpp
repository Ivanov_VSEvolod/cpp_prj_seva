//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//Знаковые и беззнаковые числа. 
//V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

#include<iostream>
#include<limits.h>
using namespace std;

int main() {
	int var = 255;
	unsigned int uVar = 255;
	cout << -var << "\t" << -uVar <<  endl;
	cout << sizeof(var) << "\t" << sizeof(uVar) << endl;
	int varMin = INT_MIN, varMax = INT_MAX;
	cout << varMin << "\t" << varMax << endl;
	unsigned uVarMin = 0, uVarMax = UINT_MAX;
	cout << uVarMin << "\t" << uVarMax << endl;
	
	return 0;
}
//Output
/*

*/
//=-===-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//end file
//-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=//

